# How to Install 

**Prerequisites**
- Make sure you have [git](https://git-scm.com/download/win) installed. 
- Make sure you have Python 3.9 or higher installed. This can also be a portable distribution like WinPython
- Have Google Chrome installed

**Installation**
- open a terminal and cd to the location you would like the project to be installed
- Clone the repository by running 
```bash
git clone https://gitlab.com/JurreCorver/stock_scraper.git
```
- install the project by running
```bash
cd stock_scraper
pip install -e .
```
You may need to point to the correct Python version, e.g. by running 
```bash
C:\Users\jiansiguo\Desktop\scraping\scraping\WPy64-31090\python-3.10.9.amd64\python.exe -m pip install -e .
```

**How to run**
- You can run the application by clicking on `run_app.bat` or by running
```bash
cd name_of_your_project
python stock_scraper/app/app.py
```





# TODO
- [x] push update script
- [x] Ensure the default data dump is placed outside of version control => we might write data to a special folder and then copy it elsewhere to prevent permission issues.
- [x] add gitignore
- [x] bash open browser on given page: explorer http://localhost:5002
- [x] test update script
- [x] relative path winpython "../WPy64-31090/python-3.10.9.amd64/python.exe"

- [ ] Give application a name and change logo/favicon
- [ ] consider creating a release branch for brother




git clone https://gitlab.com/JurreCorver/stock_scraper.git




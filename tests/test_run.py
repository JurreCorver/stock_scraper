import copy
import os
import shutil
import sys
import tempfile
import pathlib

import pandas as pd

import stock_scraper

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

tmpdir = pathlib.Path(tempfile.tempdir)

CONFIG = stock_scraper.data_classes.Config(
    OUTPUT_FILE=tmpdir / pathlib.Path("stock_data.xlsx"),
    WORKSHEET="scraped_data",
    PROD=True,
    WAIT_TIME=10,
    URL="https://xuangubao.cn/dingpan",
    SCHEDULE_PATH="test",
    UPDATE_SCRIPT='test'
)

def test_run_scraping():
    stock_scraper.run_scraping(CONFIG)

    df = pd.read_excel(CONFIG.OUTPUT_FILE)
    assert df.isna().all().all() == False


def test_aggregate():
    """
    - There is 1 tab + 1 for every col
    - Test exact output for an example input, containing duplicates and off-snap cases
    - Explicitly check the tab containing strings
    """
    # Set config

    CONFIG2 = copy.copy(CONFIG)
    CONFIG2.OUTPUT_FILE = tmpdir / pathlib.Path("test.xlsx")
    reference_path = "tests/reference_test_data_agg.xlsx"

    # Read raw data to Excel in /tmp dir
    df1 = pd.read_excel(reference_path)
    df1.to_excel(CONFIG2.OUTPUT_FILE)

    # Run aggregation logic
    stock_scraper.aggregate(CONFIG2)

    # Check number of sheets
    df2_dict = pd.read_excel(CONFIG2.OUTPUT_FILE, sheet_name=None)
    assert len(df2_dict.keys()) == 6

    # Check if the reference was fully replicated
    df3_dict = pd.read_excel(reference_path, sheet_name=None)

    comp = [df2.equals(df3) for df2, df3 in zip(df2_dict.values(), df3_dict.values())][
        1:
    ]
    assert all(comp)

import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

import datetime
from dataclasses import asdict
from typing import List

import numpy as np
import pandas as pd

def mode(items: list) -> str:
    vals, counts = np.unique(items, return_index=True)
    return vals[np.argmax(counts)]


def dt_to_sec(dt: datetime.datetime) -> int:
    """Converts a datetime to a second format."""
    return (dt.replace(tzinfo=None) - dt.min).seconds


def round_time(dt: datetime.datetime, roundTo: int = 600) -> datetime.datetime:
    """Round a datetime object to any time lapse in seconds"""
    dt = dt.to_pydatetime()
    seconds = dt_to_sec(dt)
    rounding = ((seconds + roundTo / 2) // roundTo) * roundTo
    return dt + datetime.timedelta(0, rounding - seconds, -dt.microsecond)


def select_datetimes(df: pd.DataFrame) -> pd.DataFrame:
    """
    Filter for rows that reference datetimes of interest

    Times of interest range between 9:30 to 15:00 at a 30 min
    interval.

    """
    # Create time range from 9:00 to 17:00 with stepsize 30 mins
    delta = datetime.timedelta(minutes=30)
    starting_time = datetime.datetime(year=2000, month=1, day=1, hour=9, minute=0)
    sticky_times = [(starting_time + i * delta).time() for i in range(0, 17)]

    # Create date and time columns for auxiliary purposes
    df["date"] = df.retrieval_time.map(lambda x: x.date())
    df["time"] = df.snap_times.map(lambda x: x.time())
    # Return datetimes that fit the defined timerange
    return df[df.time.isin(sticky_times)]


def remove_duplicates(df: pd.DataFrame) -> pd.DataFrame:
    """Removes potential duplicates by selecting the rows closes to the snaptime"""
    df["delta_sec"] = df.snap_times.map(
        lambda x: dt_to_sec(x.to_pydatetime())
    ) - df.retrieval_time.map(lambda x: dt_to_sec(x.to_pydatetime()))
    df["delta_sec"] = df["delta_sec"].map(lambda x: abs(x))

    return df.loc[df.groupby("snap_times")["delta_sec"].idxmin()]


def create_pivot_tables(df: pd.DataFrame, cols: List[str]) -> List[pd.DataFrame]:
    """Creates a pivot table for each column."""
    pivot_tables = [
        df.pivot_table(
            index="date",
            columns=["time"],
            values=[col],
            aggfunc=lambda x: mode(x),
        )
        for col in cols
    ]
    return pivot_tables

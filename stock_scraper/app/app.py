import datetime as dt
import json
import subprocess
from threading import Thread

import scheduler
import uvicorn
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from stock_scraper import aggregate, get_config, run_scraping

CONFIG = get_config()
with open(CONFIG.SCHEDULE_PATH, "r") as file:
    schedule_dict = json.load(file)


async def task():
    print("Running scheduled job.")
    CONFIG = get_config()
    # Scrape the website for stock aggregate data
    run_scraping(CONFIG)

    # Aggregate the data
    aggregate(CONFIG)
    # feedback
    print("Finished scheduled job.")
    return 0


def set_schedule(d, task, schedule):
    active_days = [int(day["alias"]) for day in d["day_of_week"] if day["active"]]
    active_time = [
        {"hour": int(time["name"][0:2]), "minute": int(time["name"][3:])}
        for time in d["time_of_day"]
        if time["active"]
    ]
    occurences = [
        {"day_of_week": day, "hour": time["hour"], "minute": time["minute"]}
        for day in active_days
        for time in active_time
    ]

    # schedule.cyclic(dt.timedelta(seconds=30), task)
    for occurence in occurences:
        schedule.add_job(
            func=task,
            trigger="cron",
            day_of_week=occurence["day_of_week"],
            hour=occurence["hour"],
            minute=occurence["minute"],
        )
    return 0


app = FastAPI(title="StockScraper", version="0.0.2")


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def init_data():
    global schedule
    # loop = asyncio.get_event_loop()
    schedule = AsyncIOScheduler()
    schedule.start()

    set_schedule(schedule_dict, task, schedule)


@app.get("/hello/")
async def read_root():
    """Hello World!"""
    return {"Hello": "World"}


@app.get("/run_update/")
async def run_update():
    try:
        p = subprocess.call(rf"{CONFIG.UPDATE_SCRIPT}")
        print(p)
        status = "pass"
    except Exception as e:
        status = "fail"
        print(e)

    return status


@app.get("/config/")
async def get_schedule():
    with open(CONFIG.SCHEDULE_PATH, "r") as file:
        config_dict = json.load(file)
    return config_dict


@app.put("/config/{type}/{name}")
async def put_config(type: str, name: str):
    with open(CONFIG.SCHEDULE_PATH, "r") as file:
        config_dict = json.load(file)

    for item in config_dict[type]:
        if item["name"] == name:
            item["active"] = not item["active"]

    with open(CONFIG.SCHEDULE_PATH, "w") as file:
        json.dump(config_dict, file)

    # update the scheduler
    global schedule
    schedule.remove_all_jobs()
    set_schedule(schedule_dict, task, schedule)

    return config_dict


class SPAStaticFiles(StaticFiles):
    async def get_response(self, path: str, scope):
        response = await super().get_response(path, scope)
        if response.status_code == 404:
            response = await super().get_response(".", scope)

        return response


app.mount(
    "/",
    SPAStaticFiles(directory="./stock_scraper/app//build/", html=True),
    name="Static Files",
)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=5002)

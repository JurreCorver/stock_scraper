import os
import os.path
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

import re
from dataclasses import asdict

import data_classes
import openpyxl
import toml
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import pathlib

def get_config() -> data_classes.Config:
    """Reads the config from pyproject.toml and inserts it into a dataclass."""
    config = toml.load("pyproject.toml")["configs"]
    return data_classes.Config(*config.values())


def create_driver(CONFIG: data_classes.Config):
    """Creates the javascript driver."""
    # start by defining the options
    options = webdriver.ChromeOptions()
    options.headless = CONFIG.PROD  # it's more scalable to work in headless mode
    # normally, selenium waits for all resources to download
    # we don't need it as the page also populated with the running javascript code.
    # options.page_load_strategy = 'none'
    # this returns the path web driver downloaded
    chrome_path = ChromeDriverManager().install()
    chrome_service = Service(chrome_path)
    # pass the defined options and service objects to initialize the web driver
    driver = Chrome(options=options, service=chrome_service)
    driver.implicitly_wait(CONFIG.WAIT_TIME)

    return driver


def parse_data(driver, retrieval_time: str) -> data_classes.StockPortfolioData:
    """Parse the html and enter it into a dataclass."""
    page_source = driver.page_source
    soup = BeautifulSoup(page_source, "html.parser")

    all_numbers = soup.find_all("span", class_="fluctuation-title-number")

    regex = re.compile(r"\d+")
    numbers_left = [int(nr) for nr in re.findall(regex, all_numbers[0].get_text())]
    numbers_right = [int(nr) for nr in re.findall(regex, all_numbers[1].get_text())]
    percentage_change = all_numbers[2].get_text()

    row_data = data_classes.StockPortfolioData(
        retrieval_time=retrieval_time,
        stocks_increase_nr=numbers_left[0],
        stocks_decrease_nr=numbers_left[1],
        stocks_increase_peak_nr=numbers_right[0],
        stocks_decrease_peak_nr=numbers_right[1],
        stocks_percentage=percentage_change,
    )
    return row_data


def write_excel(row_data: data_classes.StockPortfolioData, CONFIG: data_classes.Config):
    """Write the scraped data entry into an Excel file."""

    if not os.path.exists(pathlib.Path(CONFIG.OUTPUT_FILE).parent):
        os.makedirs(pathlib.Path(CONFIG.OUTPUT_FILE).parent)

    if not os.path.isfile(CONFIG.OUTPUT_FILE):
        workbook = openpyxl.Workbook()
        # workbook.create_sheet(worksheet_name)
        # worksheet = workbook[worksheet_name]
        worksheet = workbook.active
        worksheet.title = CONFIG.WORKSHEET

        worksheet.append([name for name in row_data.__annotations__.keys()])

    else:
        workbook = openpyxl.load_workbook(CONFIG.OUTPUT_FILE)
        worksheet = workbook[CONFIG.WORKSHEET]

    worksheet.append([val for val in asdict(row_data).values()])

    # To save:
    workbook.save(CONFIG.OUTPUT_FILE)

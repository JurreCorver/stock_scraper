import datetime
import time
from dataclasses import asdict

import aggregation
import data_classes
import pandas as pd
import utils


def run_scraping(CONFIG: data_classes.Config):
    """Scrape the stock-relevant info from the specified website."""
    driver = utils.create_driver(CONFIG)
    driver.get(CONFIG.URL)
    retrieval_time = time.localtime()
    retrieval_time = str(datetime.datetime.fromtimestamp(time.mktime(retrieval_time)))

    # Store local time
    time.sleep(CONFIG.WAIT_TIME)

    # Parse the data to retrieve 5 specific figures
    row_data = utils.parse_data(driver, retrieval_time)

    # Write data to Excel file
    utils.write_excel(row_data, CONFIG)


def aggregate(CONFIG: data_classes.Config):
    """Aggregate the stock data and write the pivot table to an Excel file."""
    # Read the full data from Excel
    df = pd.read_excel(CONFIG.OUTPUT_FILE)
    df.retrieval_time = pd.to_datetime(df.retrieval_time)

    # Round the datetimes
    df["snap_times"] = df.retrieval_time.map(lambda x: aggregation.round_time(x))

    # Select datetimes from 9:30-15:00 with stepsize 30 mins
    df = aggregation.select_datetimes(df)

    # Remove duplicates
    df = aggregation.remove_duplicates(df)

    # Create pivot tables
    cols = list(asdict(data_classes.StockPortfolioData(1, 1, 1, 1, 1, 1)).keys())[1:]
    pivot_tables = aggregation.create_pivot_tables(df, cols)

    # Write pivot tables to Excelsheet
    with pd.ExcelWriter(
        CONFIG.OUTPUT_FILE, mode="a", engine="openpyxl", if_sheet_exists="replace"
    ) as writer:
        for table, col in zip(pivot_tables, cols):
            table.to_excel(writer, sheet_name=col)


if __name__ == "__main__":
    # Get global config
    CONFIG = utils.get_config()

    # Scrape the website for stock aggregate data
    run_scraping(CONFIG)

    # Aggregate the data
    aggregate(CONFIG)

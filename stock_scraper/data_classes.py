from dataclasses import asdict, dataclass


@dataclass
class StockPortfolioData:
    """Dataclass containing stock info with the retrieval time."""

    retrieval_time: str
    stocks_increase_nr: int
    stocks_decrease_nr: int
    stocks_increase_peak_nr: int
    stocks_decrease_peak_nr: int
    stocks_percentage: str


@dataclass
class Config:
    """Dataclass containing the application config."""

    OUTPUT_FILE: str
    WORKSHEET: str
    PROD: bool
    WAIT_TIME: float
    URL: str
    SCHEDULE_PATH: str
    UPDATE_SCRIPT: str
    

    def __post_init__(self):
        self.PROD = bool(self.PROD)
